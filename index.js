const axios = require("axios");
const con = require('./database');
const moment = require('moment');
var listUnsave = require('./data');
async function getWordData() {
    // var listWords = await query(con, "SELECT * FROM dictionary").catch(console.log);
    var myQuery = "SELECT * FROM dictionary WHERE id IN (?)";

    var listWords = await query(con, myQuery, [listUnsave]).catch(console.log);
    var listPhonetics = [];
    var listMeanings = [];
    let indexPhonetics = 0;
    let indexMeanings = 0;
    for (let index = 0; index < listWords.length; index++) {
        console.log(index)
        const element = listWords[index];
        const req = await sendRequest("https://api.dictionaryapi.dev/api/v2/entries/en/" + element.word, 'get').catch((error) => {});
        if (req && req.status == 200) {
            if (req.data[0]) {

                req.data[0].phonetics.forEach(item => {
                    listPhonetics[indexPhonetics] = [element.id, item.text, item.audio];
                    indexPhonetics ++;
                });
                req.data[0].meanings.forEach(item => {
                    listMeanings[indexMeanings] = [element.id, item.partOfSpeech, JSON.stringify(item.definitions)];
                    indexMeanings ++;
                });
            }
        }
        if (req && req.status == 429) {
            console.log('limit');
            break;
        }
        if (index > 0 && index % 70 == 0) {
            var listPhonetics = listPhonetics.filter(function (el) {
                return el != null;
            });   
            var listMeanings = listMeanings.filter(function (el) {
                return el != null;
            });   
            var sql = "INSERT INTO phonetics (word_id, text, audio) VALUES ?";
            con.query(sql, [listPhonetics], function(err) {
                if (err) throw err;
            });
            var sql = "INSERT INTO meanings (word_id, part_of_speech, definitions) VALUES ?";
            con.query(sql, [listMeanings], function(err) {
                if (err) throw err;
            });
            listPhonetics = [];
            listMeanings = [];
            indexPhonetics = 0;
            indexMeanings = 0;
            await delay(1000 * 180);
        }
    }
    con.end();
}

async function sendRequest(url, method = 'get') {
    return new Promise((resolve, reject) => {
        axios({
            method: method,
            url: url,
        }).then(function (response) {
            // handle success
            resolve(response);
        }).catch(function (error) {
            // handle error
            reject(error)
        })
        .then(function () {
            // always executed
        });
        
    });
}

async function query(conn, q, params) {
    return new Promise((resolve, reject) => {
        const handler = (error, result) => {
            if (error) {
            reject(error);
            return;
        }
        resolve(result);
        }
        conn.query(q, params, handler);
    });
}

const delay = ms => new Promise(res => setTimeout(res, ms));

getWordData();
